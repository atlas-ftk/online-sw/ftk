#!/bin/sh

submodules=$(git submodule--helper list | awk '{print $4}')

for s in ${submodules}
do
/usr/bin/git --git-dir=${s}/.git pull
/usr/bin/git --git-dir=${s}/.git  checkout master
done

